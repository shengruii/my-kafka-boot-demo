package com.qf.my.kafka.boot.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeviceDTO {

  private String deviceKey;

  private Long LastActiveTime;
}
